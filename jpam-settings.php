<?php
add_action( 'admin_menu', 'jpam_add_admin_menu' );
add_action( 'admin_init', 'jpam_settings_init' );


function jpam_add_admin_menu(  ) {

	add_submenu_page( 'options-general.php', 'JP Ad Mailer', 'JP Ad Mailer', 'manage_options', 'jp_ad_mailer', 'jpam_options_page' );

}


function jpam_settings_init(  ) {

	register_setting( 'jpam_settings', 'jpam_settings' );

	add_settings_section(
		'jpam_settings_section',
		__( 'General Settings', 'wordpress' ),
		'jpam_settings_section_callback',
		'jpam_settings'
	);

	add_settings_field(
		'jpam_admin_email',
		__( 'Admin email(s)', 'wordpress' ),
		'jpam_admin_email_render',
		'jpam_settings',
		'jpam_settings_section'
	);


}


function jpam_admin_email_render( ) {

	$options = get_option( 'jpam_settings' );
	?>
	<input style="width: 80%;" type='text' name='jpam_settings[admin_email]' value='<?php echo $options['admin_email']; ?>'>
  <p><small>Use coma separated list of emails to send the ads to more than one admin</small></p>
	<?php

}


function jpam_settings_section_callback(  ) {

	//echo __( 'This section description', 'wordpress' );

}


function jpam_options_page(  ) {

	?>
  <div class="wrap">
	<form action='options.php' method='post'>

		<h1>JP Ad Mailer Settings</h1>
    <?php
		settings_fields( 'jpam_settings' );
		do_settings_sections( 'jpam_settings' );
		submit_button();
		?>

	</form>
</div>
	<?php

}

<?php
/*
Plugin Name: JP Ad Mailer
Description: Scrapes and emails ads created in Advanced Classifieds and Directory Pro
Version: 1.0.0
Author: Jack Karpinski / Johnston Press
Copyright: Johnston Press
*/

define('AD_POST_TYPE', 'acadp_listings');

include( plugin_dir_path( __FILE__ ) . 'jpam-settings.php');


//Activation hook
register_activation_hook(__FILE__, 'jpam_activation');
//Deactivation hook
register_deactivation_hook(__FILE__, 'jpam_deactivation');

//Send emails (via cron)
add_action('jpam_cron_mailer', 'jpam_email_ads');

//Enable HTML emails
add_filter( 'wp_mail_content_type','jpam_set_content_type' );

function jpam_activation() {
    if (! wp_next_scheduled ( 'jpam_cron_mailer' )) {
	     wp_schedule_event(time(), 'hourly', 'jpam_cron_mailer');
    }
}
function jpam_deactivation() {
	wp_clear_scheduled_hook('jpam_cron_mailer');
}

function jpam_email_ads() {
  $options = get_option( 'jpam_settings' );
  $admin_email = $options['admin_email'];

  $args = array(
  	'posts_per_page'  => 10,
  	'offset'          => 0,
  	'orderby'         => 'post_date',
  	'order'           => 'DESC',
  	'post_type'       => AD_POST_TYPE,
  	'post_status'     => 'publish',

    'meta_query'      => array(
      'relation'  => 'OR',
      array(
        'key'        => 'ad_sent_timestamp',
  	    'value'      => '1',
  	    'compare'    => '<'),
      array(
        'key'        => 'ad_sent_timestamp',
        'compare'    => 'NOT EXISTS'
      )
    )
  );

  $wp_query = new WP_Query( $args );
  if ( $wp_query->have_posts() ) {
    while( $wp_query->have_posts() ){
      $wp_query->the_post();

      //Get ad details
      $post_id = get_the_ID();
      $ad_meta = get_post_meta($post_id);
      $ad_title = get_the_title();
      $ad_url = get_permalink();
      $ad_content = get_the_content();

      //Get ad meta
      $ad_zipcode = get_post_meta($post_id, 'zipcode', true);
      $ad_address = get_post_meta($post_id, 'address', true);
      $ad_email = get_post_meta($post_id, 'email', true);
      $ad_phone = get_post_meta($post_id, 'phone', true);
      $ad_expiry = get_post_meta($post_id, 'expiry_date', true);
      $ad_price = get_post_meta($post_id, 'price', true);

      //Get user data
      $user = get_the_author();

      //Get main image
      $image_meta = get_post_meta($post_id, '_thumbnail_id', true);
      $ad_image_file = get_attached_file( $image_meta );

      $ad_image_url = get_the_post_thumbnail_url(NULL, 'full');

      //Assemble email content
      $content = '<table border="0" style="width: 100%">';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Ad ID</td><td style="padding: 5px;border:1px solid #ccc;">' . $post_id . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Add title</td><td style="padding: 5px;border:1px solid #ccc;">'. $ad_title . '</td></tr>';

      //tmx_ad_auto_title() is availbale in functions.php
      //Its purpose is to assemble auto title for front end display and create a string of ad details
      if( function_exists('tmx_ad_auto_title') ){
        $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Add details</td><td style="padding: 5px;border:1px solid #ccc;">'. tmx_ad_auto_title($post_id, true) . '</td></tr>';
      }

      //Get bike type
      $args = array(
        'name'        => 'type',
        'post_type'   => 'acadp_fields',
        'post_status' => 'publish',
        'numberposts' => 1
      );

      $bt_field = get_posts($args);
      if ( ! empty ($bt_field )){
        $bt_key = $bt_field[0]->ID;
        $bt_value = get_post_meta($post_id, $bt_key, true);
        $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Type</td><td style="padding: 5px;border:1px solid #ccc;">' . $bt_value . '</td></tr>';
      }

      //Check private/trade
      $args = array(
        'name'        => 'privatetrade',
        'post_type'   => 'acadp_fields',
        'post_status' => 'publish',
        'numberposts' => 1
      );

      $pt_field = get_posts($args);
      if ( ! empty ($pt_field )){
        $pt_key = $pt_field[0]->ID;
        $pt_value = get_post_meta($post_id, $pt_key, true);
        $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">P/T</td><td style="padding: 5px;border:1px solid #ccc;">' . $pt_value . '</td></tr>';
      }


      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Ad URL</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_url  . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Ad content</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_content . '</td></tr>';

      //Get short description
      $args = array(
        'name'        => 'short-description',
        'post_type'   => 'acadp_fields',
        'post_status' => 'publish',
        'numberposts' => 1
      );

      $sd_field = get_posts($args);
      if ( ! empty ($sd_field )){
        $sd_key = $sd_field[0]->ID;
        $sd_value = get_post_meta($post_id, $sd_key, true);
        $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Short Description</td><td style="padding: 5px;border:1px solid #ccc;">' . substr($sd_value, 0, 189) . '</td></tr>';
      }


      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Price</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_price . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Username</td><td style="padding: 5px;border:1px solid #ccc;">' . $user . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Email</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_email  . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Phone</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_phone . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Address</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_address . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Postcode</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_zipcode  . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Expires</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_expiry . '</td></tr>';
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Picture</td><td style="padding: 5px;border:1px solid #ccc;">' . $ad_image_url . '</td></tr>';



      $content .= jpam_txn_details($post_id);

      $content .= '</table>';

      //Set email subject
      $subject = 'New TMX advert: ' . $ad_title;

      //Attach the lead image
      $attachments = array($ad_image_file);

      //And send it away
      $sent = wp_mail($admin_email, $subject, $content, $headers, $attachments);

      if( $sent ){
        //ad sent, mark as sent
        update_post_meta($post_id, 'ad_sent_timestamp', time());
      }

    } //loop end
  }


}

function jpam_txn_details($id = NULL ){

  $content = NULL;

  //Is it a paid order?
  $args = array(
    'name' => 'order-listing-' . $id,
    'post_type' => 'acadp_payments',
    'numberposts' => 1
  );

  $order = get_posts($args);
  if( ! empty($order) ){
    $order_id = $order[0]->ID;

    $featured = get_post_meta($order_id, 'featured', true);
    if ($featured){
      $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Featured</td><td style="padding: 5px;border:1px solid #ccc;">YES</td></tr>';
    }

    $amount = get_post_meta($order_id, 'amount', true);
    $transaction_id = get_post_meta($order_id, 'transaction_id', true);
    $fee_plan_id = get_post_meta($order_id, 'fee_plan_id', true);
    $plan = NULL;

    if( $fee_plan_id ){

      $plan = get_post($fee_plan_id);
      $plan = $plan->post_title;

    }

    $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Price</td><td style="padding: 5px;border:1px solid #ccc;">' . $amount . '</td></tr>';
    $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Plan</td><td style="padding: 5px;border:1px solid #ccc;">' . $plan . '</td></tr>';
    $content .= '<tr><td style="padding: 5px;border:1px solid #ccc;">Transaction ID</td><td style="padding: 5px;border:1px solid #ccc;">' . $transaction_id . '</td></tr>';

  }



  return $content;


}

function jpam_set_content_type(){
    return "text/html";
}
